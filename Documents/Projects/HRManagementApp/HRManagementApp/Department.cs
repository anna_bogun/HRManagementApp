﻿using System;
namespace HRManagementApp
{
	/// <summary>
	/// Department.
	/// </summary>
	public class Department : Entity
	{
		private string name;
		private int locationId;
		private int managerId;
		[Gtk.TreeNodeValue(Column = 1)]
		public string Name
		{
			get
			{
				return name;
			}
		}
		[Gtk.TreeNodeValue(Column = 2)]
		public int LocationId
		{
			get
			{
				return locationId;
			}
		}
		[Gtk.TreeNodeValue(Column = 3)]
		public int ManagerId
		{
			get
			{
				return managerId;
			}
		}
		public override string ToString()
		{
			return string.Format("[Department: name={0}, locationId={1}, managerId={2}]", name, locationId, managerId);
		}

		public string FormatString()
		{
			return string.Format("{0}, {1}", Id, Name);
		}

		public Department(int id, string description, string name, int locationId, int managerId) : base(id, description)
		{
			this.name = name;
			this.locationId = locationId;
			this.managerId = managerId;
		}
	}
}
