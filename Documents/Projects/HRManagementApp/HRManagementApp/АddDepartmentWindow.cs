﻿using System;
using System.Windows.Forms;
namespace HRManagementApp
{
	/// <summary>
	/// Аdd department window.
	/// </summary>
	public partial class АddDepartmentWindow : Gtk.Window
	{
		public АddDepartmentWindow() :
				base(Gtk.WindowType.Toplevel)
		{
			this.Build();
		}

		protected void OnAddDepartmentClicked(object sender, EventArgs e)
		{
			try
			{
				Department dep = new Department(0,
											descriptionEntry.Text,
											 nameEntry.Text,
											Convert.ToInt32(locationEntry.Text)
											, Convert.ToInt32(managerEntry.Text));
				DBManager.updateDepartment(dep, "insert");
				this.Destroy();
				DepartmentsWindow win = new DepartmentsWindow();
				win.Show();
			}
			catch (Exception)
			{
				MessageBox.Show("Ошибка! Проверьте правильность ввода данных");
			}
		}

		protected void OnMenuClicked(object sender, EventArgs e)
		{
			this.Destroy();
			DepartmentsWindow win = new DepartmentsWindow();
			win.Show();
		}
	}
}
