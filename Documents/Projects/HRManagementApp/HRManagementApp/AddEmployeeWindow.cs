﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Gtk;
using System.Text.RegularExpressions;
namespace HRManagementApp
{
	/// <summary>
	/// GTK window. Displays field must be filled to add new Employee
	/// to data base.
	/// </summary>
	public partial class AddEmployeeWindow : Gtk.Window
	{
		public AddEmployeeWindow() :
				base(Gtk.WindowType.Toplevel)
		{
			this.Build();
			Gtk.CellRendererText ct = new Gtk.CellRendererText();
			jobCombo.PackStart(ct, false);
			jobCombo.AddAttribute(ct, "text", 0);
			List<Department> departments = DBManager.getAllDepartments();
			foreach (Department dep in departments)
			{
				departmentCombo.AppendText(dep.FormatString());
			}
		}

		protected void OnMenuClicked(object sender, EventArgs e)
		{
			this.Destroy();
			EmployeesWindow win = new EmployeesWindow();
			win.Show();
		}
		/// <summary>
		/// Processed a "Add new Employee" button click.
		/// Call updateEmployee method with "insert" parameter
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		protected void OnAddClicked(object sender, EventArgs e)
		{
			try
			{
				string job = jobCombo.ActiveText;
				int jobId = Convert.ToInt32(job.Split(',')[0]);
				string department = departmentCombo.ActiveText;
				int departmentId = Convert.ToInt32(department.Split(',')[0]);
				Employee emp = new Employee(0, descriptionEntry.Text,
										nameEntry.Text, loginEntry.Text, passwordEntry.Text, emailEntry.Text,
										phoneEntry.Text, positionEntry.Text, Convert.ToDouble(salaryEntry.Text),
										Convert.ToDateTime(dateEntry.Text),
										departmentId,
										jobId, false);
				DBManager.updateEmployee(emp, "insert");
				this.Destroy();
				EmployeesWindow win = new EmployeesWindow();
				win.Show();
			}
			catch (Exception)
			{
				MessageBox.Show("Ошибка! Проверьте правильность ввода данных");
			}
		}
	}
}
