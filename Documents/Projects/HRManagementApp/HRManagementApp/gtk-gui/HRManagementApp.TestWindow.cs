
// This file has been generated by the GUI designer. Do not modify.
namespace HRManagementApp
{
	public partial class TestWindow
	{
		protected virtual void Build()
		{
			global::Stetic.Gui.Initialize(this);
			// Widget HRManagementApp.TestWindow
			this.Name = "HRManagementApp.TestWindow";
			this.Title = global::Mono.Unix.Catalog.GetString("TestWindow");
			this.WindowPosition = ((global::Gtk.WindowPosition)(4));
			if ((this.Child != null))
			{
				this.Child.ShowAll();
			}
			this.DefaultWidth = 400;
			this.DefaultHeight = 300;
			this.Show();
		}
	}
}
