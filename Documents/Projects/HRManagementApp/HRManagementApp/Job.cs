﻿using System;
namespace HRManagementApp
{
	public class Job : Entity
	{

		private string title;
		private double minSalary;
		private double maxSalary;

		public string FormatString()
		{
			return string.Format("№{0}, {1}", Id, Title);
		}

		public Job(int id, string description, string title, double minSalary, double maxSalary) : base(id, description)
		{
			this.title = title;
			this.minSalary = minSalary;
			this.maxSalary = maxSalary;
		}
		[Gtk.TreeNodeValue(Column = 1)]
		public string Title
		{
			get
			{
				return title;
			}
		}
		[Gtk.TreeNodeValue(Column = 2)]
		public double MinSalary
		{
			get
			{
				return minSalary;
			}
		}
		[Gtk.TreeNodeValue(Column = 3)]
		public double MaxSalary
		{
			get
			{
				return maxSalary;
			}
		}

		public override string ToString()
		{
			return string.Format("[Job: title={0}, minSalary={1}, maxSalary={2}]", title, minSalary, maxSalary);
		}
	}
}

