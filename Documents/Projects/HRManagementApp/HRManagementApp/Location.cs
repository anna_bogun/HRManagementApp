﻿using System;
namespace HRManagementApp
{
	/// <summary>
	/// Location entity.
	/// </summary>
	public class Location : Entity
	{
		private string streetAddress;
		private string postalCode;
		private string city;
		private string country;


		public override string ToString()
		{
			return string.Format("[Location: streetAddress={0}, postalCode={1}, city={2}, country={3}]", streetAddress, postalCode, city, country);
		}
		[Gtk.TreeNodeValue(Column = 1)]
		public string StreetAddress
		{
			get
			{
				return streetAddress;
			}
		}
		[Gtk.TreeNodeValue(Column = 2)]
		public string PostalCode
		{
			get
			{
				return postalCode;
			}
		}

		[Gtk.TreeNodeValue(Column = 3)]
		public string City
		{
			get
			{
				return city;
			}
		}
		[Gtk.TreeNodeValue(Column = 4)]
		public string Country
		{
			get
			{
				return country;
			}
		}

		public Location(int id, string description, string streetAddress, string postalCode, string city, string country) : base(id, description)
		{
			this.streetAddress = streetAddress;
			this.postalCode = postalCode;
			this.city = city;
			this.country = country;
		}


public string FormatString()
{
	return string.Format("№{0}, {1}, {2}, {3}, {4}",
						 Id, streetAddress, city, country, postalCode); }

	}
}
