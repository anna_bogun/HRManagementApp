﻿using System;
using System.Windows.Forms;
namespace HRManagementApp
{
	public partial class DepartmentWindow : Gtk.Window
	{
		Department department;
		public DepartmentWindow(Department dep) :
				base(Gtk.WindowType.Toplevel)
		{
			this.Build();
			department = dep;
			idEntry.Text = dep.Id.ToString();
			nameEntry.Text = dep.Name;
			managerEntry.Text = dep.ManagerId.ToString();
			locationEntry.Text = dep.LocationId.ToString();
			descriptionEntry.Text = dep.Description;
		}


		protected void OnUpdateDepartmentClicked(object sender, EventArgs e)
		{
			try
			{
				Department dep = new Department(Convert.ToInt32(idEntry.Text),
											descriptionEntry.Text,
											 nameEntry.Text,
											Convert.ToInt32(locationEntry.Text)
											, Convert.ToInt32(managerEntry.Text));
				DBManager.updateDepartment(dep, "update");
				this.Destroy();
				DepartmentsWindow win = new DepartmentsWindow();
				win.Show();
			}
			catch (Exception)
			{
				MessageBox.Show("Ошибка! Проверьте правильность ввода данных");
			}
		}

		protected void OnFindManagerClicked(object sender, EventArgs e)
		{
			Employee emp = DBManager.getEmployeeById(department.ManagerId);
			EmployeeWindow win = new EmployeeWindow(emp);
			this.Destroy();
			win.Show();
		}

		protected void OnFindLocationClicked(object sender, EventArgs e)
		{
		}

		protected void OnMenuClicked(object sender, EventArgs e)
		{
            this.Destroy();
DepartmentsWindow win = new DepartmentsWindow();
win.Show();
		}
	}
}
