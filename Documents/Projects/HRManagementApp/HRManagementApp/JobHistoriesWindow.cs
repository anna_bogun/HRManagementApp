﻿using System;
using System.Collections.Generic;
namespace HRManagementApp
{
	/// <summary>
	/// Job histories window.
	/// </summary>
	public partial class JobHistoriesWindow : Gtk.Window
	{
		Employee employee;
		Gtk.NodeStore store;
		List<JobHistory> list;
		Gtk.NodeStore Store
		{
			get
			{
				if (store == null)
				{
					store = new Gtk.NodeStore(typeof(JobHistory));
					foreach (JobHistory jh in list)
					{
						store.AddNode(jh);
					}
				}
				return store;
			}
		}

		public JobHistoriesWindow(List<JobHistory> jlist, Employee emp) :
				base(Gtk.WindowType.Toplevel)
		{
			this.Build();
			employee = emp;
			list = jlist;
			SetSizeRequest(200, 150);
			view.NodeStore = Store;
			int i = 0;
			view.AppendColumn("№", new Gtk.CellRendererText(), "text", i++);
			view.AppendColumn("Дата начала работы", new Gtk.CellRendererText(), "text", i++);
			view.AppendColumn("Дата окончания работы", new Gtk.CellRendererText(), "text", i++);
			view.AppendColumn("№ сотрудника", new Gtk.CellRendererText(), "text", i++);
			view.AppendColumn("№ профессии", new Gtk.CellRendererText(), "text", i++);
			view.AppendColumn("№ отдела", new Gtk.CellRendererText(), "text", i++);
			view.ShowAll();
		}

		protected void OnReturnToEmployeeClicked(object sender, EventArgs e)
		{
			this.Destroy();
			EmployeeWindow win = new EmployeeWindow(employee);
            win.Show();
		}
	}
}
