﻿using System;
namespace HRManagementApp
{
	/// <summary>
	/// Job history entity. Just contains information from database.
	/// Extends class Entity
	/// see cref="HRManagementApp.Entity"/>
	/// </summary>
	public class JobHistory : Entity
	{
		private DateTime startDate;
		private DateTime endDate;
		private int employeeId;
		private int jobId;
		private int departmentId;
		[Gtk.TreeNodeValue(Column = 1)]
		public string StartDate
		{
			get
			{
				return startDate.ToString("yyyy-MM-dd");
			}
		}
		[Gtk.TreeNodeValue(Column = 2)]
		public string EndDate
		{
			get
			{
				return endDate.ToString("yyyy-MM-dd");
			}
		}
		[Gtk.TreeNodeValue(Column = 3)]
		public int EmployeeId
		{
			get
			{
				return employeeId;
			}
		}
		[Gtk.TreeNodeValue(Column = 4)]
		public int JobId
		{
			get
			{
				return jobId;
			}
		}
		[Gtk.TreeNodeValue(Column = 5)]
		public int DepartmentId
		{
			get
			{
				return departmentId;
			}
		}
		public JobHistory(int id, string description, DateTime startDate, DateTime endDate, int employeeId, int jobId, int departmentId) : base(id, description)
		{
			this.startDate = startDate;
			this.endDate = endDate;
			this.employeeId = employeeId;
			this.jobId = jobId;
			this.departmentId = departmentId;
		}

		public override string ToString()
		{
			return string.Format("[JobHistory: startDate={0}, endDate={1}, employeeId={2}, jobId={3}, departmentId={4}, StartDate={5}, EndDate={6}, EmployeeId={7}, JobId={8}, DepartmentId={9}]", startDate, endDate, employeeId, jobId, departmentId, StartDate, EndDate, EmployeeId, JobId, DepartmentId);
		}
	}
}
