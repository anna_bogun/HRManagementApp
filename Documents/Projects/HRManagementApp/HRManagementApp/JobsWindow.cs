﻿using System;
namespace HRManagementApp
{
	public partial class JobsWindow : Gtk.Window
	{
		public JobsWindow() :
				base(Gtk.WindowType.Toplevel)
		{
			this.Build();
		}

		protected void OnFindJobClicked(object sender, EventArgs e)
		{
		}

		protected void OnAddJobClicked(object sender, EventArgs e)
		{
		}

		protected void OnMenuClicked(object sender, EventArgs e)
		{
		}
	}
}
